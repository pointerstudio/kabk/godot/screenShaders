@tool
@icon("./assets/PlaneMesh.svg")
extends "res://usefulScripts/nodes/BasicShape.gd"
class_name BasicPlane3D

func new_mesh() -> PrimitiveMesh:
	return PlaneMesh.new()
	
func new_shape() -> Shape3D:
	return BoxShape3D.new()
