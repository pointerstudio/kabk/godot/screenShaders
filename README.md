[link to minitutorial](https://pointer.click/files/minitutorial_godot_screenShaders.mp4)

Disclaimer, the sound is not great in this video! I discovered that the footsteps were super loud in comparison to my voice. I hope it is still useful. If this is disturbing, please let me know :-)

Any kind of feedback is immensely motivating by the way to make something easier accessible or useful for you.
